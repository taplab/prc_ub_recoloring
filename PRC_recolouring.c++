/*
In this version I am jumping Topo2 (soft) region randomly
at a user-defined rate
*/
#include<iostream>
#include<math.h>
#include<stdlib.h>
#include<fstream>
#include<sstream>
#include<vector>
#include<ctime>
#include <unistd.h>
#include<functional>
#include<numeric>
#include<cstdlib>
#include <algorithm>
using namespace std;

int mass[10];
int Ntot,ntype;
double Lmaxx,Lminx,Lmaxy,Lminy,Lmaxz,Lminz;
int timet;
double Lx,Ly,Lz;
string dummy;
int nangles,nangletypes,nbonds,nbondtypes;

//INPUT PARAMETERS
int nattempts;
double alpha;
double rmax;
//

int id,mol,type;
double x,y,z;
int ix,iy,iz;
double vx,vy,vz;
//
const int N=1;
const int Nmax=2000;

const int Nbeads=1000;
const int Nprot=20;

double Polymer[N][Nmax][6]; //x,y,z,mol,type,id
double PolymerWrap[N][Nmax][3]; //x,y,z
int PolymerImg[N][Nmax][3]; //ix,iy,iz

double Protein[Nmax][6]; //x,y,z,mol,type,id
double ProteinWrap[Nmax][3]; //x,y,z,mol,type,id
double ProteinImg[Nmax][3]; //x,y,z,mol,type,id

double Velocity[N][Nmax][3];
int Angles[Nbeads][4]; //bead1,bead2,bead3,atype
int Bonds[Nbeads+1][3]; //bead1,bead2,btype


double distance(double v1[3], double v2[3]);
int main(int argc, char* argv[]){
cout << " ############################# EXTERNAL PRC ################### " <<endl;
int seedr=atoi(argv[7]);
srand(time(NULL)*seedr);
double pp=rand();

nattempts=atof(argv[4]); //number of recolouring attempts
alpha=atof(argv[5]); //recruitment
rmax=atof(argv[6]); //cut off distance
int time=atoi(argv[1]);
//ARGV[1]=timestep
//ARGV[2]=infile-no time
//ARGV[3]=outfile-no time

//WRITE LEF position
stringstream writeFileD;
writeFileD <<"Recolouring_"<<argv[2]<<"dat";
ofstream writed;
writed.open(writeFileD.str().c_str(), std::ios_base::app);

//READ FILE
ifstream indata;
stringstream readFile;
readFile.str("");
readFile.clear();
readFile << argv[2] << argv[1];
indata.open(readFile.str().c_str());
cout << readFile.str().c_str()<<endl;
if(!indata.good()){cout << "AHAHAHAHHA"<<endl; cin.get();cin.get();cin.get();}

//read 10 lines
for(int i=0;i<12;i++){
if(i==2) {
indata >> Ntot;
//cout << "Ntot " << Ntot<<endl;
}
if(i==3){
indata >> ntype;
//cout << "Ntypes: " << ntype<<endl;
}
if(i==4){
indata >> nbonds;
//cout << "Nbonds: " << nbonds<<endl;
}
if(i==5){
indata >> nbondtypes;
//cout << "Nbondtypes: " << nbondtypes<<endl;
}
if(i==6){
indata >> nangles;
//cout << "Nangles: " << nangles<<endl;
}
if(i==7){
indata >> nangletypes;
//cout << "Nangletypes: " << nangletypes<<endl;
}
if(i==9) {
indata >> Lminx >> Lmaxx;
//cout << "L " << Lminx<< " " << Lmaxx <<endl;
Lx = Lmaxx-Lminx;
//cout << "Lx " << Lx <<endl;
}
if(i==10) {
indata >> Lminy >> Lmaxy;
//cout << "L " << Lminy << " " << Lmaxy <<endl;
Ly = Lmaxy-Lminy;
//cout << "Ly " << Ly <<endl;
}
if(i==11) {
indata >> Lminz >> Lmaxz;
//cout << "L " << Lminz << " " << Lmaxz <<endl;
Lz = Lmaxz-Lminz;
//cout << "Lz " << Lz <<endl;
}
else{
getline(indata,dummy);
//cout<<"d"<<dummy<<endl;
}
}

while(1==1){
getline(indata,dummy);
string str=dummy;
string str2 ("Atoms");
if (str.find(str2) != string::npos)break;
}

//READ ATOMS
for(int n=0; n<Ntot; n++){
	indata >> id>> mol>>type>> x>>y>>z>>ix>>iy>>iz;
	//cout << id <<" " << x<<endl;cin.get();
	if(type==5){
	ProteinWrap[id-1-Nbeads][0]=x; //we are interested to the wrapped coord pbc
	ProteinWrap[id-1-Nbeads][1]=y; //we are interested to the wrapped coord pbc
	ProteinWrap[id-1-Nbeads][2]=z; //we are interested to the wrapped coord pbc
	ProteinImg[id-1-Nbeads][0]=ix;
	ProteinImg[id-1-Nbeads][1]=iy;
	ProteinImg[id-1-Nbeads][2]=iz;
	//
	Protein[id-1-Nbeads][0]=x+Lx*ix;
	Protein[id-1-Nbeads][1]=y+Ly*iy;
	Protein[id-1-Nbeads][2]=z+Lz*iz;
	Protein[id-1-Nbeads][3]=mol;
	Protein[id-1-Nbeads][4]=type;
	Protein[id-1-Nbeads][5]=id;
	}
	else{
	PolymerWrap[0][id-1][0]=x;
	PolymerWrap[0][id-1][1]=y;
	PolymerWrap[0][id-1][2]=z;
	PolymerImg[0][id-1][0]=ix;
	PolymerImg[0][id-1][1]=iy;
	PolymerImg[0][id-1][2]=iz;
	//
	Polymer[0][id-1][0]=x+Lx*ix;
	Polymer[0][id-1][1]=y+Ly*iy;
	Polymer[0][id-1][2]=z+Lz*iz;
	Polymer[0][id-1][3]=mol;
	Polymer[0][id-1][4]=type;
	Polymer[0][id-1][5]=id;
	}
}

//READ VELOCITIES
indata >> dummy;
//cout << "dum " << dummy<<endl;
for(int n=0; n<Ntot; n++){
	indata >> id >> vx>>vy>>vz;
	//cout << id <<" " << vx<<endl;
	Velocity[0][id-1][0]=vx;
	Velocity[0][id-1][1]=vy;
	Velocity[0][id-1][2]=vz;
}

//READ BONDS
int nbonds0=Nbeads;
int bid,btype,b1,b2;
bid=0;btype=0;b1=0;b2=0;
for(int n=0;n<nbonds;n++)for(int p=0;p<3;p++)Bonds[n][p]=0;
indata >> dummy;
//cout << "dum " << dummy<<endl;
for(int nb=0; nb<nbonds; nb++){
	indata >> bid >> btype>>b1>>b2;
	//cout << bid <<" " << b1 << " " <<b2<<endl;
	Bonds[bid-1][0]=b1;
	Bonds[bid-1][1]=b2;
	Bonds[bid-1][2]=btype;
}

//READ ANGLES
int aid,atype,a1,a2,a3;
aid=0;atype=0;a1=0;a2=0;a3=0;
for(int na=0;na<nangles;na++)for(int pa=0;pa<4;pa++)Angles[na][pa]=0;
indata >> dummy;
//cout << "dum " << dummy<<endl;
for(int na=0; na<nangles; na++){
	indata >> aid >> atype>>a1>>a2>>a3;
	//cout << aid <<" " << a1 << " " <<a2<<endl; cin.get();
	Angles[na][0]=a1;
	Angles[na][1]=a2;
	Angles[na][2]=a3;
	Angles[na][3]=atype;
}

/*
RECOLOR BY PRC
 1. Pick an atom of chromatin at random
 2. with prob alpha - check neighbours
 3. if there is a protein (type 5) recolour to 1 (ub) or 6 (h3k27+ub)
 */
if (time>=0){
int pbound=0;

//loop over attempts
for (int n=0;n<nattempts;n++){

	int i = rand() % Nbeads;
	int itype=Polymer[0][i][4];
	double prec=rand()*1.0/RAND_MAX;

	if(prec<alpha){
	//recruited - look through proteins if there is one nearby
		for (int j=0;j<Nprot;j++){
			double dx=PolymerWrap[0][i][0]-ProteinWrap[j][0];
			double dy=PolymerWrap[0][i][1]-ProteinWrap[j][1];
			double dz=PolymerWrap[0][i][2]-ProteinWrap[j][2];
			if(dx>Lx/2.)dx-=Lx; if(dx<-Lx/2.)dx+=Lx;
			if(dy>Ly/2.)dy-=Ly; if(dy<-Ly/2.)dy+=Ly;
			if(dz>Lz/2.)dz-=Lz; if(dz<-Lz/2.)dz+=Lz;
 			double dd=dx*dx+dy*dy+dz*dz;
            //cout << "Looking at beads " <<i << " and " << j << " @ " << dd <<endl;
            if(dd>rmax)continue;
            //THIS PROTEIN IS NEAR TO BEAD THAT WANTS RECOLOURING
            int jtype=Protein[j][4];
            pbound=0;
	    //CHECK IF PROTEIN IS BOUND (HAS TYPE 4 OR 6 NEARBY ITSELF)
	    for (int k=0;k<Nbeads;k++){
		int ktype=Polymer[0][k][4];
        if(ktype==4||ktype==6){
        double ddx=PolymerWrap[0][k][0]-ProteinWrap[j][0];
		double ddy=PolymerWrap[0][k][1]-ProteinWrap[j][1];
		double ddz=PolymerWrap[0][k][2]-ProteinWrap[j][2];
        if(ddx>Lx/2.)ddx-=Lx; if(ddx<-Lx/2.)ddx+=Lx;
		if(ddy>Ly/2.)ddy-=Ly; if(ddy<-Ly/2.)ddy+=Ly;
		if(ddz>Lz/2.)ddz-=Lz; if(ddz<-Lz/2.)ddz+=Lz;
		double ddd=ddx*ddx+ddy*ddy+ddz*ddz;
		if(ddd<rmax){pbound=1;}
        }
	    }
            cout << "Looking at beads " << i << " type " << itype << " and " << j << " type " << jtype << " at distance " << dd << " < " << rmax <<endl;
            if(itype!=4&&itype!=6&&pbound==1){Polymer[0][i][4]=1; writed << time << " " << i << " " << itype << " " << 1 << endl;}
            else if(itype==4&&pbound==1){Polymer[0][i][4]=6; writed << time << " " << i << " " << itype << " " << 6 << endl;}
		}

	}
	else{
	//noisy
        if(itype==1){if(rand()%2==0){Polymer[0][i][4]=2;writed << time << " " << i << " " << itype << " " << 2 << endl;}}
        else if(itype==6){if(rand()%2==0){Polymer[0][i][4]=4;writed << time << " " << itype <<" " << 6 << " " << 4 << endl;}}
	}

}
} //if time
///////////////////////////////////////////////////////////////////
///////////////////	WRITE NEW FILE  /////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

//OUTFILE
stringstream writeFile;
writeFile <<argv[3]<<argv[1];
ofstream write(writeFile.str().c_str());
cout << "writing on .... " <<writeFile.str().c_str()<<endl;
write.precision(19);
write << "LAMMPS data file via write_data, version 15 May 2015, timestep = "<< argv[1]<< endl;
write <<endl;
write << Ntot  << " atoms "<<endl;
write << ntype  << " atom types "<<endl;
//write<< 10 << " extra bond per atom " <<endl;
write << nbonds << " bonds "<<endl;
write << nbondtypes << " bond types "<<endl;
write << nangles << " angles "<<endl;
write << nangletypes << " angle types "<<endl;
write << "\n";
write << Lminx << " " << Lmaxx << " xlo xhi"<<endl;
write << Lminy << " " << Lmaxy << " ylo yhi"<<endl;
write << Lminz << " " << Lmaxz << " zlo zhi"<<endl;
write << "\nMasses \n"<<endl;
for(int j=0; j<ntype;j++) write << j+1 << " " << 1<< endl;

write << "\nAtoms \n"<<endl;
for(int n=0; n<Nbeads;n++) write << Polymer[0][n][5] << " " << Polymer[0][n][3] << " " <<  Polymer[0][n][4] << " " << PolymerWrap[0][n][0] << " " <<PolymerWrap[0][n][1] << " " << PolymerWrap[0][n][2] << " " << PolymerImg[0][n][0] << " " << PolymerImg[0][n][1] << " " << PolymerImg[0][n][2] << endl;

for(int n=0; n<Nprot;n++) write << Protein[n][5] << " " << Protein[n][3] << " " <<  Protein[n][4] << " " << ProteinWrap[n][0] << " " <<ProteinWrap[n][1] << " " << ProteinWrap[n][2] << " " << ProteinImg[n][0] << " " << ProteinImg[n][1] << " " << ProteinImg[n][2] << endl;
	
write << "\n\n\nVelocities\n"<<endl;
for(int n=0; n<Ntot;n++) write << n+1 << " " << Velocity[0][n][0] << " "<< Velocity[0][n][1] << " "<<  Velocity[0][n][2] << endl;


write << "\nBonds \n"<<endl;
for(int n=0; n<nbonds;n++) {
write << n+1 << " " << Bonds[n][2] << " " << Bonds[n][0] << " " << Bonds[n][1] << endl;
}

write << "\nAngles \n"<<endl;
for(int na=0; na<nangles;na++){
write << na+1 << " " << Angles[na][3] << " " << Angles[na][0] << " " << Angles[na][1] << " " << Angles[na][2]<< endl;
//if(Angles[na][1]==1){cout<<"Angles AHAHA "<<na+1<<" "<<Angles[na][0]<< " " <<Angles[na][1] << endl; cin.get();cin.get();}

}
cout << " ###################################### FIN ############################## " <<endl;

return 0;

}

double distance(double v1[3], double v2[3]){
double d=0;

d=(v1[0]-v2[0])*(v1[0]-v2[0])+(v1[1]-v2[1])*(v1[1]-v2[1])+(v1[2]-v2[2])*(v1[2]-v2[2]);

return sqrt(d);
}
