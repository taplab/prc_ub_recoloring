Example to run LAMMPS with an external program that performs a "recoloring" of bead that are 3D proximal to a protein of interest.
In this particular example, PRC proteins deposit ubiquitin marks on chromatin.  

run.lam is the LAMMPS script (tested with LAMMPS version 3Mar20) 

PRC_recolouring.c++ is the c++ external code called within LAMMPS to perform the recoloring 

StartFiles is a folder containing equilibrated initial configurations to run the simulation
